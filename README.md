# C++ graph

## Description

A quick & dirty dependency graph generator for C++ projects.

It only parses C++ files for includes and classes declaration. There is no code parsing using `libgcc` nor `libclang`.

## Disclaiming

Please, be lenient: it's an old tool I excavate from my Python 2.x era ^_^

## License

This project is distributed under the WTFPL 2 or later (see LICENSE for details).

## Requirement

* a 3.x python interpreter.

## Usage

Note that the used files hierarchy may make it fail: it considers the symmetry between namespaces and directories. It should also work with source using `lib/core`, `lib/ui` and `tools/my-powerful-program` but should fail with `include`, `src`, and `prog`.

### To have a "nice" PDF

Graph is only for public dependencies, no the private ones.

```sh
$ cpp-graph.py project-root .../main.cpp dot output.dot
```

or:

```sh
$ cpp-graph.py project-root .../main.cpp dotns output.dot
```

and to generate the PDF:

```sh
$ dot data.dot -Tpdf -o data.pdf
```

It strongly depends on your project's architectural volition O:-.

### Other commands

to print dependency count for each file:

```sh
$ cpp-graph.py project-root .../main.cpp print
```

to print dependency detail for each file:

```sh
$ cpp-graph.py project-root .../main.cpp printfull
```

to print number of files depending on their parent namespace:

```sh
$ cpp-graph.py project-root .../main.cpp dot output.dot
```

