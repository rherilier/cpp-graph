#!/usr/bin/env python3

# Copyright @ 2024 Rémi Hérilier
#
# SPDX-License-Identifier: WTFPL

import os
import sys
import re
import queue

##############################################################################
# some const
#
include_test = re.compile('#include [<"][^>"]+[>"]')
include_regex = re.compile('#include [<"]([^>"]+)[>"]')
clean_regex = re.compile('.*/(include|src)/(.*)$')
class_decl_regex = re.compile('class\s+(\S+)(\s*:\s*public\s+(\S+))')

##############################################################################
# useful
#
def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

##############################################################################
# some rewriting rules
#
rewriting_rules = [
    ("Qt", "Qt[A-Z]", "Qt"),
    ("errno", None, "C"),
    ("fcntl", None, "C"),
    ("libgen", None, "C"),
    ("stdlib", None, "C"),
    ("sys", "^sys/", "C"),
    ("unistd", None, "C"),
]

def fix_ns(modname, implname = None):
    if len(modname.split(os.sep)) != 1:
        return modname

    for tag, regex, prefix in rewriting_rules:
        if regex:
            if re.match(regex, modname):
                return prefix + os.sep + modname
        elif modname.startswith(tag):
            return prefix + os.sep + modname

    # fix C++ std (the remainding files)
    if implname:
        return os.path.dirname(implname) + os.sep + modname
    elif modname[0].islower():
        return "std" + os.sep + modname

    return modname

##############################################################################
# argv checking
#
output = None

if len(sys.argv) >= 4:
    ROOTDIR = os.path.abspath(sys.argv[1])
    input = os.path.abspath(sys.argv[2])
    command = sys.argv[3]
    if len(sys.argv) == 5:
        output = sys.argv[4]
else:
    eprint("usage: {} project-root main.cpp (print|printfull|stat|dot output.dot|dotns output.dot)".format(os.path.basename(sys.argv[0])))
    sys.exit(1)

CWD = os.getcwd()

if os.path.isdir(ROOTDIR):
    os.chdir(ROOTDIR)
else:
    eprint("'{}' is not a directory".format(ROOTDIR))
    usage(2)

srcs = []

if command != "print" and command != "printfull" and command != "stat" and command != "dot" and command != "dotns":
    eprint("unknown action: '{}'".format(command))
    sys.exit(3)

for name in os.listdir(ROOTDIR):
    if "build" in name:
        continue
    if name.startswith("."):
        continue
    if os.path.isdir(name):
        srcs.append(os.path.relpath(name))

if os.path.isfile(input):
    input = os.path.relpath(input)
else:
    eprint("'{}' is not a file".format(input))
    sys.exit(4)

##############################################################################
# def get_module_info()
#
def get_module_info(filename):
    tmp = clean_regex.match(filename)
    if tmp:
        info = os.path.splitext(tmp.group(2))
    else:
        info = os.path.splitext(filename)

    modname = info[0]
    ext = info[1]
    headername = None
    implname = None
    known = False

    if os.path.isfile(filename):
        return (os.path.splitext(filename)[0], None, filename, True)

    name = modname + ".h"
    if os.path.isfile(name):
        headername = name
    else:
        for src in srcs:
            path = os.path.join(src, "include", name)
            if os.path.isfile(path):
                known = True
                headername = path

    if headername == None:
        name = modname + ".h"
        if os.path.isfile(name):
            headername = name
        else:
            for src in srcs:
                path = os.path.join(src, "include", name)
                if os.path.isfile(path):
                    known = True
                    headername = path

    name = modname + ".cpp"
    if os.path.isfile(name):
        implname = name
    else:
        for src in srcs:
            path = os.path.join(src, "src", name)
            if os.path.isfile(path):
                known = True
                implname = path

    # fix module name
    modname = fix_ns(modname, implname)

    return (modname, headername, implname, known)

##############################################################################
# class module
#
class module:
    def __init__(self, modname, headerpath, implpath, known):
        self.fullname = modname
        path = modname.split(os.sep)
        self.name = path[-1]
        self.ns = path[0:-1]
        self.header = headerpath
        self.impl = implpath;
        self.known = known
        self.inherit = None
        self.public = []
        self.private = []
        self.rpublic = []
        self.rprivate = []
        if (headerpath):
            self.__parse_header(headerpath)
        if (implpath):
            self.__parse_impl(implpath)

    def print(self, indent = 0, detail = False):
        print("|   " * indent + "file: '{}'".format(self.fullname))
        if self.header:
            print("|   " * indent + "|   header: '{}'".format(self.header))
        if self.impl:
            print("|   " * indent + "|   impl: '{}'".format(self.impl))
        if self.inherit:
            print("|   " * indent + "|   inherit: '{}'".format(self.inherit))
        if self.public and detail:
            print("|   " * indent + "|   public : '{}'".format(self.public))
        elif self.public:
            print("|   " * indent + "|   public : {} deps".format(len(self.public)))
        if self.private and detail:
            print("|   " * indent + "|   private: '{}'".format(self.private))
        elif self.private:
            print("|   " * indent + "|   private : {} deps".format(len(self.private)))
        if self.rpublic and detail:
            print("|   " * indent + "|   rpublic : '{}'".format(self.rpublic))
        elif self.rpublic:
            print("|   " * indent + "|   rpublic : {} deps".format(len(self.rpublic)))
        if self.rprivate and detail:
            print("|   " * indent + "|   rprivate: '{}'".format(self.rprivate))
        elif self.rprivate:
            print("|   " * indent + "|   rprivate : {} deps".format(len(self.rprivate)))

    def print_dot(self, file, indent, with_ns = False):
        node = '"{}"'.format(self.fullname.replace(os.sep, "::"))
        if self.known:
            color = "green"
        else:
            color = "blue"
        file.write('  ' * indent + '{} [label={},shape="box",color="{}"];\n'.format(node, node, color))
        return [(node, [s.replace(os.sep,"::") for s in list(set(self.public + self.private))])]

    def fix(self, modtree):
        self.public = [s.replace(".h", "") for s in self.public]
        self.private = [s.replace(".h", "") for s in self.private]

        self.public = self.__fix_dep(self.public)
        self.private = self.__fix_dep(self.private)

        if self.fullname in self.private:
            self.private.remove(self.fullname)

        for entry in self.private:
            if entry in self.public:
                self.public.remove(entry)

        self.public = [modtree.nearest(s) for s in self.public]
        self.private = [modtree.nearest(s) for s in self.private]

        if self.inherit:
            if len(self.inherit.split(os.sep)) == 1:
                res = tree.find(self.inherit)
                if res:
                    self.inherit = res.fullname

    def init_reverse(self, tree):
        for entry in self.public:
            modname = entry.replace(os.sep, "::")
            node = tree.get(entry)
            if node:
                node.__add_pub_rev(self.fullname)
            else:
                eprint("{} not found".format(entry))
        for entry in self.private:
            modname = entry.replace(os.sep, "::")
            node = tree.get(entry)
            if node:
                node.__add_priv_rev(self.fullname)
            else:
                eprint("{} not found".format(entry))

    def __parse_header(self, filename):
        with open(filename, newline='') as file:
            for line in file:
                result = include_regex.match(line)
                if result:
                    self.public.append(fix_ns(result.group(1)))
                if "class" in line:
                    result = class_decl_regex.search(line)
                    if result:
                        self.inherit = result.group(3).replace("::", os.sep)

    def __parse_impl(self, filename):
        with open(filename, newline='') as file:
            for line in file:
                result = include_regex.match(line)
                if result:
                    self.private.append(fix_ns(result.group(1)))

    def __fix_dep(self, dep):
        res = []
        for entry in dep:
            if len(entry.split(os.sep)) == 1:
                desc = tree.find(entry)
                if desc:
                    res.append(desc.fullname)
                else:
                    res.append(entry)
            else:
                res.append(entry)
        return sorted(res)

    def __add_pub_rev(self, name):
        self.rpublic.append(name)

    def __add_priv_rev(self, name):
        self.rprivate.append(name)

##############################################################################
# class namespace
#
class namespace:
    def __init__(self, fullname = None):
        self.fullname = fullname
        if fullname:
            self.name = fullname.split(os.sep)
        self.ns = {}

    def has(self, modname):
        path = modname.split(os.sep)
        if len(path) == 1:
            return path[0] in self.ns
        elif path[0] in self.ns:
            return self.ns[path[0]].has(os.sep.join(path[1:]))
        else:
            return False

    def add(self, modname, desc, accum = []):
        self.__add(modname, desc, [])

    def find(self, modname):
        if modname in self.ns:
            return self.ns[modname]
        else:
            for key in self.ns:
                if type(self.ns[key]) is namespace:
                    res = self.ns[key].find(modname)
                    if res:
                        return res
            return None

    def get(self, modname):
        path = modname.split(os.sep)
        key = path[0]
        if key in self.ns:
            if type(self.ns[key]) is namespace:
                return self.ns[key].get(os.sep.join(path[1:]))
            else:
                return self.ns[key]

    def nearest(self, name):
        for key in self.ns:
            if type(self.ns[key]) is namespace:
                ret = self.ns[key].nearest(name)
                if ret:
                    return ret;
            elif self.ns[key].fullname.endswith(name):
                return self.ns[key].fullname
        return None

    def print(self, indent = 0, detail = True):
        if self.fullname:
            print("|   " * indent + "namespace: {}".format(self.name))
        else:
            print("|   " * indent + "namespace: [root]")

        for key in sorted(self.ns.keys()):
            self.ns[key].print(indent + 1, detail)

    def print_stat(self, indent = 0):
        name = self.fullname or "All"
        print("|   " * indent + "{}: {} file(s)".format(name, self.get_module_stat()))

        for key in sorted(self.ns.keys()):
            if type(self.ns[key]) is namespace:
                self.ns[key].print_stat(indent + 1)

    def print_dot(self, file, indent = 0, with_ns = False):
        offset = 1
        outer = []
        if self.fullname and with_ns:
            fullname = self.fullname.replace(os.sep, "_")
            file.write ('  ' * indent + 'subgraph "cluster_{}" {{\n'.format(fullname))
            file.write ('  ' * indent + '  label="{}"\n'.format(self.fullname))
            file.write ('  ' * indent + '  color=red\n')
            file.write ('  ' * indent + '  ordering=out\n')
            offset = 2
        for key in self.ns:
            res = self.ns[key].print_dot(file, indent + 1, with_ns)
            if res:
                outer += res
        if self.fullname and with_ns:
            file.write ('  ' * indent + '}\n')
        return outer

    def get_module_stat(self):
        count = 0
        for key in self.ns:
            if type(self.ns[key]) is namespace:
                count += self.ns[key].get_module_stat()
            else:
                count += 1
        return count

    def fix(self, tree):
        for key in self.ns:
            self.ns[key].fix(tree)

    def init_reverse(self, tree):
        for key in self.ns:
            self.ns[key].init_reverse(tree)

    def __add(self, modname, desc, accum = []):
        path = modname.split(os.sep)
        if len(path) == 1:
            mod = path[0]
            self.ns[mod] = desc
        else:
            hns = path[0]
            accum.append(hns)
            sns = path[1:]
            if hns not in self.ns:
                self.ns[hns] = namespace("::".join(accum))
            self.ns[hns].__add(os.sep.join(sns), desc, accum)

##############################################################################
# main
#
files = queue.Queue()
tree = namespace()

files.put(input)

while not files.empty():
    file = os.path.relpath(files.get())
    (name, header, impl, known) = get_module_info(file)
    if tree.has(name):
        continue

    desc = module(name, header, impl, known)
    tree.add(desc.fullname, desc)
    for entry in desc.public:
        files.put(entry)
    for entry in desc.private:
        files.put(entry)

tree.fix(tree)
tree.init_reverse(tree)

os.chdir(CWD)

if command == "print" or command == "printfull":
    tree.print(detail = command == "printfull")
elif command == "stat":
    tree.print_stat()
elif command == "dot" or command == "dotns":
    with open(output, "w") as file:
        file.write("""digraph "dependencies" {
label="dependencies graph";
splines=true;
ratio=fill;
size="33.11,46.81";
page="33.11,46.81";
rankdir=LR;
nodesep=0.5;
""")

        res = tree.print_dot(file, with_ns = command == "dotns")

        for (node, dep) in res:
            if dep:
                labels = " ".join(['"{}"'.format(s) for s in dep])
                file.write('  {} -> {{ {} }};\n'.format(node, labels))

        file.write('}\n')
